import java.util.Scanner;

public class Uitdaging {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Conversietabel °C naar °F");
        System.out.println("=========================");

        System.out.print("Geef de begintemperatuur in °C: ");
        int begin = kb.nextInt();
        System.out.print("\nGeef de eindtemperatuur in °C: ");
        int eind = kb.nextInt();
        System.out.print("\nGeef de stapwaarde: ");
        int stap = kb.nextInt();
        System.out.println("===============");
        System.out.println("|-------------|");
        System.out.println("|  °C  |  °F  |");
        int huidig = begin;
        while (huidig <= eind) {
            double result = (double)huidig * 1.8 + 32;
            System.out.printf("|%5d | %-5.1f|\n", huidig, result);
            huidig+=stap;
        }
        System.out.println("===============");

    }
}
