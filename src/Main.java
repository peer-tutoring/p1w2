import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int waarde;
        int keuze;

        System.out.println("Conversie graden Celcius - Fahrenheit");
        System.out.println("=====================================");

        System.out.println("Welke conversie wens je te doen?");
        System.out.println("\t1)°C naar °F");
        System.out.println("\t2)°F naar °C");
        System.out.println("Uw keuze?");
        keuze = kb.nextInt();
        while (keuze == 1 || keuze == 2) {
            System.out.println("\t2)°F naar °C");
            System.out.println("Uw keuze?");
            keuze = kb.nextInt();
            if (keuze == 1) {
                System.out.println("geef de waarde in °C");
                waarde = kb.nextInt();
                double result = (double) waarde * (1.8) + 32;
                System.out.println(waarde + "°C = " + result + "°F");
            }
            if (keuze == 2) {
                System.out.println("geef de waarde in °F");
                waarde = kb.nextInt();
                double result = (double) (waarde - 32) / 1.8;
                System.out.println(waarde + "°C = " + result + "°F");
            }
        }
        System.out.println("Tot ziens!");
    }
}
