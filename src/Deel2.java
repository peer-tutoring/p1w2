import java.util.Scanner;

public class Deel2 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Conversietabel °C naar °F");
        System.out.println("=========================");

        System.out.print("Geef de begintemperatuur in °C: ");
        int begin = kb.nextInt();
        System.out.print("Geef de eindtemperatuur in °C: ");
        int eind = kb.nextInt();
        if(begin >= eind){
            System.out.println("De begintemperatuur moet kleiner zijn dan de eindtemperatuur!");
            System.exit(1);
        }
        System.out.println("Geef de stapwaarde: ");
        int stap = kb.nextInt();

        int huidig = begin;
        while (huidig <= eind) {
            double result = (double)huidig * 1.8 + 32;
            System.out.printf("%d°C = %.1f°F\n", huidig, result);
            huidig+=stap;
        }

    }
}
